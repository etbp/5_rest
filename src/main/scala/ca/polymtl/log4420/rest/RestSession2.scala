package ca.polymtl.log4420
package rest

import model.Cheminement

import net.liftweb._
import common._
import common.Box._
import http.rest.RestHelper
import json.JsonAST.JValue

import util.BasicTypesHelpers._

object RestSession2 extends RestHelper
{
  serve( "cheminement" :: Nil prefix {

    /*
    * Suprime la ième session
    */
    case id :: "session" :: i :: Nil  JsonDelete _ => ???

    /*
    * Deplacer cours
    */
    case id :: Nil JsonPut _ => ???
  })
}
