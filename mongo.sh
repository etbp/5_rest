#!/bin/sh

cd /tmp
wget http://fastdl.mongodb.org/linux/mongodb-linux-x86_64-2.2.1.tgz
tar -xzf mongodb-linux-x86_64-2.2.1.tgz
rm mongodb-linux-x86_64-2.2.1.tgz
cd mongodb-linux-x86_64-2.2.1/bin
mkdir -p /tmp/mongodb
./mongod --dbpath /tmp/mongodb